# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :handico_book,
  ecto_repos: [HandicoBook.Repo]

# Configures the endpoint
config :handico_book, HandicoBook.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8y2KjwPo47FOcuwdQhOTCMLrMJdcQSMzy2NyofEprpGbqmDzGYH7P94YLFo7P/EV",
  render_errors: [view: HandicoBook.ErrorView, accepts: ~w(html json)],
  pubsub: [name: HandicoBook.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Custom config
config :handico_book, device_path: File.read!("device_path")


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
