defmodule HandicoBook.CardReader do
  use GenServer
  require Logger

  @delay 60
  @character_map ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "\n"]

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, self(), opts)
  end

  def init(_pid) do
    exec = "cat #{Application.fetch_env!(:handico_book, :device_path)}"
    Port.open({:spawn, exec}, [:binary, :exit_status])
    {:ok, ""}
  end

  def handle_info({_port, {:data, data}}, state) do
    new_state = state <> data
    case new_state do
      rfid when byte_size(rfid) == 176 ->
        clean_rfid =
          rfid
          |> to_char_list
          |> Enum.chunk(16)
          |> Enum.map(fn(x) -> Enum.at(x, 2) end)
          |> Enum.take(10)
          |> Enum.reduce("", fn(x, acc) ->
            acc <> Enum.at(@character_map, x - 0x1E) end)

        # Send card to Main Library
        send_card_info(clean_rfid)
        {:noreply, ""}
#
      _ ->
        {:noreply, new_state}
    end
  end

  def handle_info({_port, {:exit_status, status}}, _state) do
    Logger.info "RFIDReader Exit status: #{status}. Shutdown in #{@delay} seconds."
    Process.sleep(@delay)
    exit(:shutdown)
  end

  def send_card_info(clean_rfid) do
    server = Application.get_env(:handico_book, :phoenix_book_server)
    pass = Application.get_env(:handico_book, :server_pass)
    options = [ssl: [{:versions, [:'tlsv1.2']}], recv_timeout: 500]
    body = "{
              \"card_number\": \"#{clean_rfid}\",
              \"password\": \"#{pass}\"
            }"
    HTTPoison.post "#{server}/api/card_info", body, [{"Content-Type", "application/json"}], options
  end
end
